const productList = {
  products: [
    {
      title: "Table",
      imageUrl: "https://images.demandware.net/dw/image/v2/BBBV_PRD/on/demandware.static/-/Sites-master-catalog/default/dw56ed2c33/images/530000/533587.jpg?sfrm=jpg",
      price: 20,
      description: "A table is very Beutifull",
    },
    {
      title: "Bag",
      imageUrl: "https://tse1.mm.bing.net/th?id=OIP.JXklXtjFe6r6JJMttRQL5wHaHa&pid=Api&P=0",
      price: 20,
      description: "A bag is very Beutifull",
    },
  ],
  render() {
    const renderHook = document.getElementById("app");
    const prodlist = document.createElement("ul");
    prodlist.className = "product-item";
    for (const prod of this.products) {
      const prodEl = document.createElement("li");
      prodEl.className = "product-item";
      prodEl.innerHTML = `
      <div>
            <img src= "${prod.imageUrl}" alt="product-image">
            <div class="product_item_content">
                <h2>${prod.title}</h2>
                <h2>${prod.price}</h2>
                <p>${prod.description}</p>
                <button>Add to Cart</button>
            </div>
        </div>
      `
      prodlist.append(prodEl);
      renderHook.append(prodlist);
    }
  },
};
productList.render();
